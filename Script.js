
$(document).ready(function () {
    $("#btn-search").on("click", function () {
        onBtnClickSearchWeather();
    });
});
function onBtnClickSearchWeather() {
    "use strict";
    var searchValue = {
        city: ''
    }
    readDataOnInput(searchValue);
    var vKiemTra = validateInput(searchValue);
    if (vKiemTra) {
        callApiServerRequestCurrent(searchValue);
    }
};

function readDataOnInput(paramSearchValue) {
    paramSearchValue.city = $("#place-inp").val();
    console.log(paramSearchValue.city);
};

function validateInput(paramSearchValue) {
    if (paramSearchValue.city == '') {
        alert('Ô search còn trống, vui lòng kiểm tra!');
        return false;
    }
    return true;
}

function callApiServerRequestCurrent(paramSearchValue) {
    $.ajax({
        url: 'http://api.openweathermap.org/data/2.5/weather?q=' + paramSearchValue.city + "&units=metric" +
            "&APPID=c10bb3bd22f90d636baa008b1529ee25",
        type: "GET",
        dataType: "json",
        success: function (responseJson) {
            console.log(responseJson);
            handleResult(responseJson);
        },
        error: function (ajaxContext) {
            alert(ajaxContext.responseText)
        }

    });
};

function handleResult(paramResponseJson) {
    $("#test").show()
    $("#test").html(
        `<h3 class="text-center">Current Weather for ${paramResponseJson.name}, ${paramResponseJson.sys.country}</h3>
        <b>Weather:</b> ${paramResponseJson.weather[0].main} <br>
        <b>Description:</b> <img src='http://openweathermap.org/img/w/${paramResponseJson.weather[0].icon}.png'> ${paramResponseJson.weather[0].description}  <br>
        <b>Temperature:</b> ${paramResponseJson.main.temp} &#8451; <br>
        <b>Pressure:</b> ${paramResponseJson.main.pressure} hpa  <br>
        <b>Humidity:</b> ${paramResponseJson.main.humidity}%</h3>  <br>
        <b>Min Temperature:</b> ${paramResponseJson.main.temp_min} &#8451;  <br>
        <b>Max Temperature:</b> ${paramResponseJson.main.temp_max} &#8451;  <br>
        <b>Wind Speed:</b> ${paramResponseJson.wind.speed} m/s  <br>
        <b>Wind Direction:</b> ${paramResponseJson.wind.deg} &#176;`)
}


