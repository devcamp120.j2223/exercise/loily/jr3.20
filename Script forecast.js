
var gStt = 0;
$(document).ready(function () {
    $("#search-btn").on("click", function () {
        onBtnClickSearchWeather()
    });
    function onBtnClickSearchWeather() {
        var vWeatherForecast = {
            cityName: '',
            cityDay: '',
        }
        readInputData(vWeatherForecast);
        var vKiemTra = validateForecast(vWeatherForecast);
        if (vKiemTra) {
            callApiServerRequestForecast(vWeatherForecast);
        }
    }

    function readInputData(paramWeatherForecast) {
        paramWeatherForecast.cityName = $("#city-name").val().trim();
        paramWeatherForecast.cityDay = $("#city-day").val();
        console.log(paramWeatherForecast.cityDay);
    }

    function validateForecast(paramWeatherForecast) {
        if (paramWeatherForecast.cityName === "") {
            alert("chưa nhập city name!");
            return false;
        };
        if (paramWeatherForecast.cityDay === "") {
            alert("chưa nhập city day!");
            return false;
        };
        return true;
    };

    function callApiServerRequestForecast(paramWeatherForecast) {
        $.ajax({
            url: 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + paramWeatherForecast.cityName + "&units=metric" +
                "&cnt=" + paramWeatherForecast.cityDay + "&APPID=c10bb3bd22f90d636baa008b1529ee25",
            type: "GET",
            dataType: "json",
            success: function (responseJson) {
                console.log(responseJson);
                loadDataToTable(responseJson);
            },
        });
    };

    function loadDataToTable(paramgDataWeather) {
        for (var i = 0; i < paramgDataWeather.list.length; i++) {
            $(paramgDataWeather).each(function (index, input) {
                $("#table-inp tbody").append(
                    "<tr><td>" + input.cnt +
                    "</td><td>" + `<img src="http://openweathermap.org/img/w/` + input.list[i].weather[0].icon + `.png""` +
                    "</td><td>" + input.list[i].weather[0].main +
                    "</td><td>" + input.list[i].weather[0].description +
                    "</td><td>" + input.list[i].temp.morn +
                    "</td><td>" + input.list[i].temp.night +
                    "</td><td>" + input.list[i].temp.min +
                    "</td><td>" + input.list[i].temp.max +
                    "</td><td>" + input.list[i].pressure +
                    "</td><td>" + input.list[i].humidity +
                    "</td><td>" + input.list[i].speed +
                    "</td><td>" + input.list[i].deg +
                    "</td></tr>"
                )
            });
        }
    }
});
